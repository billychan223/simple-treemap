import { useForm } from "react-hook-form";


export type FormValues = {
    data: string | null;
    row: number | null;
}

interface Props {
    onSubmitForm: (data: FormValues) => void
}

export default function InputForm({onSubmitForm}:Props) {
    const { register, handleSubmit, formState: {errors} } = useForm<FormValues>();
    const onSubmit = (data: FormValues) => onSubmitForm(data);
    return (
      <form onSubmit={handleSubmit(onSubmit)}>
        <h3> data </h3>
        <textarea   id="data"
        {...register("data",{required: true, validate: value => {
            if (value===null)return "Json is required";
            try {
                const arrays = [...JSON.parse(value)];
                if(arrays.length > 50) {
                    return "length should be smaller than 50"
                }
                if(arrays.some((ele) => ele.name.length > 50)) {
                    return "some names are over 50 characters"
                }
                if(arrays.some((ele) => !Number.isInteger(ele.weight))){
                    return "some weights are not integer"
                }
            } catch (e) {
                return "Invalid Json";
            }
            return true;
        }})
    }/>
        {errors?.data && <span>{errors?.data?.message}</span>}

        <h3> row </h3>
        <input type={"number"}
        {...register("row",{required: true, valueAsNumber: true})} />
        {errors?.row && <span>row is required</span>}
        
        <input type="submit" />
      </form>
    );
}