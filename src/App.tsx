
import { createContext, useState } from 'react';
import InputForm, { FormValues } from './InputForm/form';
import TreeMap from './Treemap/Treemap';

export const TreeMapDataContext = createContext<FormValues>({data: null, row: null});

function App() {
  const [data ,setData] = useState<FormValues>({data: null, row: null});

  function updateData(data: FormValues) {
    setData(data);
  }

  return (
    <div className="App">
      <TreeMapDataContext.Provider value={data}>
        <InputForm onSubmitForm={updateData}/>
        <TreeMap/>
      </TreeMapDataContext.Provider>

    </div>
  );
}

export default App;
