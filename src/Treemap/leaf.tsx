

interface Props {
    name: string,
    weight: number,
    value: number,
    height: number
    rowWeight: number
}

const Leaf = ({name,weight, value,height, rowWeight}: Props) => {

    return <div style={{ backgroundColor: value>=0? "green": "red", height: `${height}px`, width: `${(weight/rowWeight) * 100}%`, outline:"1px solid black"}}>
        <h4>{name}</h4>
        {value*100}%
    </div>
}

export default Leaf