import React, { useContext } from "react"
import { TreeMapDataContext } from "../App"
import Leaf from "./leaf"
import { getSortedTreeMapArray } from "./util/array"

const TreeMap = () => {
    const leafHeight = 100
    const values = useContext(TreeMapDataContext)

    if(values.data===null || values.row===null) return null
    const leafArr = [...JSON.parse(values.data ?? "")]
    const totalWeight = leafArr.reduce((accum, current) => accum + current.weight, 0)
    leafArr.sort((a,b) => (a.weight > b.weight) ? -1: 1)
    const rowWeight = Math.ceil(totalWeight / values.row)
    const sortedArr = getSortedTreeMapArray(leafArr,values.row).flat()
    return <>
    <h3> graph </h3>
    <div style={{flexWrap: "wrap", display: "flex",backgroundColor:"white", height: `${leafHeight * values.row}px`}}>
        {sortedArr.map((data,index) => <Leaf {...data} rowWeight={rowWeight} height={leafHeight} key={index}/>)}
    </div>
    </>
}

export default TreeMap

