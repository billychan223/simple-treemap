export function firstCombination(array: any[], target: number) {
  const local: any[] = [];
  return uniqueCombination({ start: 0, sum: 0, target, local, array });
}

export function uniqueCombination({
  start,
  sum,
  target,
  local,
  array,
}: {
  start: number;
  sum: number;
  target: number;
  local: any[];
  array: any[];
}) {
  for (let i = start; i < array.length; i++) {
    // Check if the sum exceeds K
    if (sum + array[i].weight > target) continue;

    // Check if it is repeated or not
    if (i > start && array[i].weight === array[i - 1].weight) continue;

    // Take the element leto the combination
    local.push(array[i]);

    uniqueCombination({
      start: i + 1,
      sum: sum + array[i].weight,
      target,
      local,
      array,
    });

    if (
      local.reduce((total, current) => total + current.weight, 0) === target
    ) {
      return local;
    }
  }
}

export function removeElementOfWeight(array: any[], weight: number) {
  const index = array.map((item) => item.weight).indexOf(weight);
  if (index > -1) {
    array.splice(index, 1);
  }
}

export function getSortedTreeMapArray(treeMapArr: any[], row: number) {
  const result = [];
  treeMapArr.sort((a, b) => b.weight - a.weight);

  while (!(treeMapArr.length === 0)) {
    let sum = treeMapArr.reduce((total, current) => total + current.weight, 0);
    var target =
      Math.ceil(sum / row) > treeMapArr[0].weight
        ? Math.ceil(sum / row)
        : treeMapArr[0].weight;
    // 3. find first combination of target
    let combination = firstCombination(treeMapArr, target);
    // 4. remove elements if found, else reduce target
    if (combination) {
      for (const element of combination) {
        removeElementOfWeight(treeMapArr, element.weight);
      }
      result.push(combination);
      row -= 1;
    } else {
      target -= 1;
    }
  }
  console.log(result);
  return result;
}
